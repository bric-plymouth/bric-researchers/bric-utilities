# sshAzureVM.ps1
# Author: Julien Besle 01/11/2022
# 
# Connects to Azure virtual machine (VM) from a PowerShell terminal through SSH.
# If the VM is not yet running, starts the VM and waits for it to accept SSH connections
# When disconnecting from the SSH connection, the script stops the VM (Only if it had started it, 
# not if the VM was already running when the script was called. This means that disconnecting
# from the the first Powershell terminal that used this script will terminate all the other SSH connections)
# 

$ResourceGroupName = "RG-BRIC-RESEARCHER-XXXX"
$VMname = "vm-bric-researcher-Xxxxx"
$IPaddress = "XX.XXX.XX.XX"
$UserName = "xxxxxxx"


$timedOut = $false

# First check if the VM is already running
Write-Host "Checking if VM $VMname is already running:"
$cmd = "Get-AzVm -ResourceGroupName $ResourceGroupName -Status"
echo $cmd
$VM_status = Invoke-Expression $cmd

# If not, then start it
if ($VM_status.PowerState -eq "VM running")
{
	Write-Host "VM ${VMname} is already running"
	echo ""
}
else
{
	Write-Host "VM ${VMname} is not currently running"
	echo ""
	$stopVM = "Y" # if the VM was not running, then it should be stopped at the end of this script
	
	Write-Host "Starting the VM:"
	$cmd = "Start-AzVM -Name $VMname -ResourceGroupName $ResourceGroupName"  # Start the VM
	echo $cmd
	Invoke-Expression $cmd
	# Check that it worked
	$cmd = "Get-AzVm -ResourceGroupName $ResourceGroupName -Status"
	echo $cmd
	$VM_status = Invoke-Expression $cmd
	if ($VM_status.PowerState -ne "VM running")
	{
		Write-Host "Could not start VM $VMname"	
		Exit
	}
	Write-Host "Started the VM"
	echo ""

	# Check that the VM is accepting connections before attempting to connect through SSH
	$waitTime = 15
	Write-Host "Waiting ${waitTime}s for connections to be available..."
	Start-Sleep $waitTime
	Write-Host "Testing the connection to the VM:"
	$timeOut = 120 # time out in seconds
	$startTime = get-date
	$cmd = "test-netconnection $IPaddress -Port 22"
	echo $cmd
	$TcpTest = Invoke-Expression $cmd
	while (-not $TcpTest.TcpTestSucceeded)
	{
		$currentTime = $($(get-date) - $startTime).TotalSeconds
		Write-Host "The VM not yet available for connections ($currentTime s after it started)"
		if ($currentTime -gt $timeOut)
		{
			$timedOut = $true
			Write-Host "Connecting to the VM timed out"
			$stopVM = Read-Host "Would you like to stop and deallocate the VM (Y/N, default = Y)?"
			if (stopVM -eq "")
			{
				$stopVM = "Y"
			}
			break
		}
		echo $cmd
		$TcpTest = Invoke-Expression $cmd
	}
	Write-Host "The VM is now accepting connections"
	echo ""
}

# Connect to the VM
if (-not $timedOut)
{
	while($stopAndExit -ne 'Y') # connect in a loop until told to stop the VM (this is to prevent the VM stopping from unexpected disconnections
	{
		Write-Host "Connecting to the VM:"
		
		$cmd = "`$env:DISPLAY=`"127.0.0.1:0.0`"" # Set X11 display to be local-host
		echo $cmd
		Invoke-Expression $cmd
		
		$cmd = "ssh -Y ${UserName}@$IPaddress"   # connect to VM with X11 forwarding
		echo $cmd
		Invoke-Expression $cmd

		Write-Host "Disconnected from the VM" # This only executes after exiting the ssh connection
		echo ""
		
		if ($stopVM -eq "Y")
		{
			$stopAndExit = Read-Host "Would you like to stop and deallocate the VM (Y/N), default = Y)?"
			if ($stopAndExit -eq "")
			{
				$stopAndExit = "Y"
			}
		}
		else
		{
			$stopAndExit = "Y"
		}
	}

}

# echo $stopVM
if ($stopVM -eq "Y")
{ # only stop the VM if it was started in this script (Not great. It would be preferable to probe the VM for any current connection and only stop it if this is the last remaining connection)
	Write-Host "Stopping the VM:"
	$cmd = "Stop-AzVM -Name $VMname -ResourceGroupName $ResourceGroupName -Force"  # Stop the VM
	echo $cmd
	Invoke-Expression $cmd
	
	Write-Host "Stopped and deallocated VM $VMname"
	echo ""
}
else
{
	Write-Host "VM $VMname IS STILL RUNNING"
	Start-Sleep 2
	echo ""
}
